// Данное задание не обязательно для выполнения
//
// Задание
// Реализовать функцию, которая позволит оценить, успеет ли команда разработчиков сдать проект до наступления дедлайна.
//
// Технические требования:
// Функция на вход принимает три параметра:
// 1. массив из чисел, который представляет скорость работы разных членов команды. Количество элементов в массиве
// означает количество человек в команде. Каждый элемент означает сколько стори поинтов (условная оценка сложности
// выполнения задачи) может выполнить данный разработчик за 1 день.
// 2. массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить).
// Количество элементов в массиве означает количество задач в беклоге. Каждое число в массиве означает количество
// стори поинтов, необходимых для выполнения данной задачи.
// 3. дата дедлайна (объект типа Date).
// После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из
// беклога до наступления дедлайна (работа ведется начиная с сегодняшнего дня). Если да, вывести на экран
// сообщение: Все задачи будут успешно выполнены за ? дней до наступления дедлайна!. Подставить нужное число
// дней в текст. Если нет, вывести сообщение Команде разработчиков придется потратить дополнительно ? часов
// после дедлайна, чтобы выполнить все задачи в беклоге
// Работа идет по 8 часов в день по будним дням
console.log('-------------------- JS_Homework#20_optional. Deadline estimation function --------------------');
console.log('');

function deadlineEstimation(team, backlog, deadline) {
    let productivityADay = 0;
    let i;
    for (i = 0; i < team.length; i++) {
        productivityADay += team[i];
    }
    let allStorypoints = 0;
    for (let j = 0; j < backlog.length; j++) {
        allStorypoints += +backlog[j];
    }
    // productivityADay = 20; // (сторипоинтов за 8 часов) Размерность - по условию задачи. Если устал разбираться со случайными массивами, раскомментируй эту строку!
    // allStorypoints = 200; // (сторипоинтов) Размерность - по условию задачи. Если устал разбираться со случайными массивами, раскомментируй эту строку!
    let daysWeNeed = Math.round(allStorypoints / productivityADay); // Количество дней, необходимых для завершения проекта.
    let allWorkingDays = Math.round((deadline - Date.now()) / 1000 / 60 / 60 / 24 / 7 * 5); // Определяем количество всех рабочих дней, оставшихся до дэдлайна.
    let workingDaysToDeadline = Math.round(allWorkingDays - daysWeNeed);
    console.log('Deadline: ' + deadline.toDateString(), '|', 'All Working Days We Have: ' + allWorkingDays, '|', 'Days We Need: ' + daysWeNeed, '|', 'Working Days To Deadline: ' + workingDaysToDeadline, '|', 'Persons In Our Team: ', i); // Статистика
    console.log('');
    if (workingDaysToDeadline >= 0) {
        return `Все задачи будут успешно выполнены за ${workingDaysToDeadline} дней до наступления дедлайна!`;
    } else { // ?
        return `Команде разработчиков придется потратить дополнительно ${Math.abs(workingDaysToDeadline) * 8} часов после 
        дедлайна, чтобы выполнить все задачи в беклоге.`;
    }
}

// Функция make1DArrayRandom() возвращает 1D-массив, в котором количество элементов и их значения генерируются
// случайным образом (но в установленных пределах, которые задаются при вызове функции):
function make1DArrayRandom(quantityOfElements, valueOfElements) {
    let elementOfArray;
    let array = [];
    for (let i = 0; i < parseInt(Math.random() * quantityOfElements + 2); i++) {
        elementOfArray = parseInt(Math.random() * valueOfElements + 1);
        array[i] = elementOfArray;
    }
    return array;
}

let teamProductivityArr = make1DArrayRandom(6, 11); // Создаем массив производительности команды.
let backlogArr = make1DArrayRandom(30, 200); // Создаем массив сложности проекта. Чтобы сделать backlog проще, нужно уменьшить значения.
let deadline = new Date('2019-07-15'); // Устанавливаем дэдлайн проекта.
console.log(`Массив производительности команды: ........... ${teamProductivityArr}`);
console.log(`Массив сложности проекта (бэклог-массив): .... ${backlogArr}`);
console.log(`Дэдлайн: ..................................... ${deadline.toDateString()}`);
console.log('');
console.log(deadlineEstimation(teamProductivityArr, backlogArr, deadline));
console.log('');
console.log('-------------------- JS_Homework#20_optional. Deadline evaluating function --------------------');